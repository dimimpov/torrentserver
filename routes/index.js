const getTorrents = require("./getTorrents/getTorrentRoute");
const authUrl = require("./getAuthUrl/getAuthUrlRoute");

const callBackUrl = require("./callBack/callBackRoute");

module.exports = [].concat(getTorrents, authUrl, callBackUrl);
