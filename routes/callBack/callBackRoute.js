const callBackRoute = {
	method: "GET",
	path: "/oauth2callback",
	handler: require("./callBackHandler")
};

module.exports = callBackRoute;
