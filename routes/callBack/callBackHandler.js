const googleAuth = require("../../googleDrive/auth");
const redis = require("redis");
const config = require("config");

module.exports = async function (request, h) {
	const client = redis.createClient(config.get("app.redis").port, config.get("app.redis").host);
	const code = request.query.code;
	const { tokens } = await googleAuth.getToken(code);
	//add exp time
	client.set("key", JSON.stringify(tokens), "EX", 200);
	const response = h.response({ token: tokens });
	client.get("key", function (err, reply) {
		console.log(reply);
	});
	response.type("application/json");
	return response;
};
