const AuthRoute = {
	method: "GET",
	path: "/auth",
	handler: require("./getAuthUrlHandler")
};

module.exports = AuthRoute;
