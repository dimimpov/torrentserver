const googleAuth = require("../../googleDrive/auth");
const config = require("config");

module.exports = async function (request, h) {
	const url = googleAuth.generateAuthUrl({
		access_type: "offline",
		scope: config.get("app.googleApi").scopes.join(" ")
	});
	const response = h.response({ url: url });
	response.type("application/json");
	return response;
};
