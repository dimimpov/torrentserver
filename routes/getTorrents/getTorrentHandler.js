const Torrent = require("../../models/torrent");

module.exports = async function (request, h) {
	const torrents = await Torrent.findAll();
	const response = h.response(torrents);
	response.type("application/json");
	return response;
};
