const torrentsRoute = {
	method: "GET",
	path: "/",
	handler: require("./getTorrentHandler")
};

module.exports = torrentsRoute;
