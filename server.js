const Hapi = require("hapi");
const server = new Hapi.Server({ host: "localhost", port: 3000 });
const plugins = require("./plugins");
const routes = require("./routes/index");
server.route(routes);

const startServer = async function() {
  try {
    // add things here before the app starts, like database connection check etc
    await server.register(plugins);
    // {
    //   register: require("hapi-plugin-pg"),
    //   options: {
    //     connectionString: `postgres: //${config.get("app.database").name}:${
    //       config.get("app.database").password
    //     }@${config.get("app.database").host}:${
    //       config.get("app.database").port
    //     }/${config.get("app.database").name}`
    //   }
    // },
    // err => {
    //   if (err) {
    //     throw err;
    //   }
    // }

    await server.start();
    console.log("server is running");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
server.table().forEach(route => console.log(`${route.method}\t${route.path}`));
const start = async () => {
  try {
    await startServer();
  } catch (error) {
    throw error;
  }
};

start();
module.exports = server;
