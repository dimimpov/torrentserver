const Sequelize = require("sequelize");
const config = require("config");

const sequelize = new Sequelize(
	config.get("app.database").database,
	config.get("app.database").username,
	config.get("app.database").password,
	{
		dialect: config.get("app.database").dialect,
		host: config.get("app.database").host,
		port: config.get("app.database").port
	}
); // sequelize instance

const Torrent = sequelize.define(
	"Torrent",
	{
		url: Sequelize.STRING
	},
	{
		freezeTableName: true,
		timestamps: false
	}
);

module.exports = Torrent;
