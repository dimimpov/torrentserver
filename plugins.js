const config = require("config");
const Sequelize = require("sequelize");

let plugins = [];
const db = {
  plugin: require("hapi-sequelizejs"),
  options: [
    {
      name: config.get("app.database").name, // identifier
      // ignoredModels: [__dirname + "/server/models/**/*.js"], // OPTIONAL: paths/globs to ignore files
      sequelize: new Sequelize(
        config.get("app.database").database,
        config.get("app.database").username,
        config.get("app.database").password,
        {
          dialect: config.get("app.database").dialect,
          host: config.get("app.database").host,
          port: config.get("app.database").port
        }
      ), // sequelize instance
      sync: false, // sync models - default false
      forceSync: false // force sync (drops tables) - default false
    }
  ]
};

plugins.push(db);

module.exports = plugins;
