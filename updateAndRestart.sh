#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
 rm -rf torrentserver

#  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
# sudo apt-get install nodejs

# clone the repo again
git clone git@gitlab.com:dimimpov/torrentserver.git
#source the nvm file. In an non
#If you are not using nvm, add the actual path like



pm2 kill

sudo npm remove pm2 -g

sudo npm install pm2 -g

pm2 status

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.

cd torrentserver

#install npm packages
echo "Running npm install"
    npm install

#Restart the node server
 pm2 start server.js

exit