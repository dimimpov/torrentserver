const { google } = require("googleapis");
const config = require("config");

const oAuth2Client = new google.auth.OAuth2(
	config.get("app.googleApi").clientId,
	config.get("app.googleApi").clientSecret,
	config.get("app.googleApi").redirectUrl
);

// TODO : Check if we have previously stored a token.
module.exports = oAuth2Client;
